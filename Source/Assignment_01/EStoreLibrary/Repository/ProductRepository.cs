﻿using EStoreLibrary.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;

namespace EStoreLibrary.Repository
{
    public class ProductRepository : IProductRepository
    {

        public Product GetProductByID(int productId) => ProductManagement.Instance.GetProductByID(productId);

        public IEnumerable<Product> GetProducts() => ProductManagement.Instance.GetProductList();
        public IEnumerable<Category> GetCategorys() => ProductManagement.Instance.GetCategoryList();

        public void InsertProduct(Product product) => ProductManagement.Instance.AddNew(product);

        public void DeleteProduct(Product product) => ProductManagement.Instance.Remove(product);

        public void UpdateProduct(Product product) => ProductManagement.Instance.Update(product);

    }
}
