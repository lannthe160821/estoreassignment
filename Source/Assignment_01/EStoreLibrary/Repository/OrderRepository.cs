﻿using EStoreLibrary.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EStoreLibrary.Repository
{
    public class OrderRepository : IOrderRepository
    {
        public Order GetOrderByID(int orderId) => OrderManagement.Instance.GetOrderByID(orderId);

        public IEnumerable<Order> GetOrders() => OrderManagement.Instance.GetOrderList();

        public void InsertOrder(Order order) => OrderManagement.Instance.AddNew(order);

        public void DeleteOrder(Order order) => OrderManagement.Instance.Remove(order);

        public void UpdateOrder(Order order) => OrderManagement.Instance.Update(order);

    }
}
