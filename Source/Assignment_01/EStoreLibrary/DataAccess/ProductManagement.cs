﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;

namespace EStoreLibrary.DataAccess
{
    public class ProductManagement
    {

        private static ProductManagement instance = null;

        private static readonly object instanceLock = new object();

        private ProductManagement() { }

        public static ProductManagement Instance
        {
            get
            {
                lock (instanceLock)
                {
                    if (instance == null)
                    {
                        instance = new ProductManagement();
                    }
                    return instance;
                }
            }
        }

        public IEnumerable<Product> GetProductList()
        {
            List<Product> products;
            try
            {
                var myStockDB = new eStoreContext();
                products = myStockDB.Products.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return products;
        }

        public IEnumerable<Category> GetCategoryList()
        {
            List<Category> category;
            try
            {
                var myStockDB = new eStoreContext();
                category = myStockDB.Categories.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return category;
        }

        public Product GetProductByID(int ProductID)
        {
            Product? product = null;
            try
            {
                var myStockDB = new eStoreContext();
                product = myStockDB.Products.SingleOrDefault(product => product.ProductId == ProductID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return product;
        }

        public void AddNew(Product product)
        {
            try
            {
                
                    var myStockDB = new eStoreContext();
                    myStockDB.Products.Add(new Product
                    {
                        CategoryId = product.CategoryId,
                        ProductName = product.ProductName,
                        Weight = product.Weight,
                        UnitPrice = product.UnitPrice,
                        UnitInStock = product.UnitInStock
                    });

                    myStockDB.SaveChanges();
               
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Update(Product product)
        {
            try
            {
                Product _Product = GetProductByID(product.ProductId);
                if (_Product != null)
                {
                    var myStockDB = new eStoreContext();
                    myStockDB.Entry<Product>(product).State = EntityState.Modified;
                    myStockDB.SaveChanges();
                }
                else
                {
                    throw new Exception("The Product does not already exist");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Remove(Product product)
        {
            try
            {
                Product c = GetProductByID(product.ProductId);
                if (c != null)
                {
                    var myStockDB = new eStoreContext();
                    myStockDB.Products.Remove(product);
                    myStockDB.SaveChanges();
                }
                else
                {
                    throw new Exception("The Product does not already exist");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


       

    }
}
