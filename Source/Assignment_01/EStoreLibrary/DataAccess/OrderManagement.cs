﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EStoreLibrary.DataAccess
{
    public class OrderManagement
    {
        private static OrderManagement instance = null;

        private static readonly object instanceLock = new object();

        private OrderManagement() { }

        public static OrderManagement Instance
        {
            get
            {
                lock (instanceLock)
                {
                    if (instance == null)
                    {
                        instance = new OrderManagement();
                    }
                    return instance;
                }
            }
        }

        public IEnumerable<Order> GetOrderList()
        {
            List<Order> orders;
            try
            {
                var myStockDB = new eStoreContext();
                orders = myStockDB.Orders.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return orders;
        }

        

        public Order GetOrderByID(int orderID)
        {
            Order? order = null;
            try
            {
                var myStockDB = new eStoreContext();
                order = myStockDB.Orders.SingleOrDefault(order => order.OrderId == orderID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return order;
        }

        public void AddNew(Order order)
        {
            try
            {
                var myStockDB = new eStoreContext();
                myStockDB.Orders.Add(new Order
                {
                   MemberId = order.MemberId,
                   OrderDate = order.OrderDate,
                   RequiredDate = order.RequiredDate,
                   ShippedDate = order.ShippedDate,
                   Freight = order.Freight,
                });

                myStockDB.SaveChanges();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Update(Order order)
        {
            try
            {
                Order _order = GetOrderByID(order.OrderId);
                if (_order != null)
                {
                    var myStockDB = new eStoreContext();
                    myStockDB.Entry<Order>(order).State = EntityState.Modified;
                    myStockDB.SaveChanges();
                }
                else
                {
                    throw new Exception("The Order does not already exist");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Remove(Order order)
        {
            try
            {
                Order c = GetOrderByID(order.OrderId);
                if (c != null)
                {
                    var myStockDB = new eStoreContext();
                    myStockDB.Orders.Remove(order);
                    myStockDB.SaveChanges();
                }
                else
                {
                    throw new Exception("The Order does not already exist");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
