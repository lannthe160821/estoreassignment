﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EStoreLibrary.DataAccess
{
    public class MemberManagement
    {
        private static MemberManagement? instance = null;
        public static readonly object instanceLock = new object();
        private MemberManagement() { }  
        public static MemberManagement Instance
        {
            get
            {
                lock (instanceLock)
                {
                    if (instance == null)
                    {
                        instance = new MemberManagement();
                    }
                    return instance;
                }
            }
        }


        public bool Login(string email, string password)
        {
            try
            {
                if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password))
                {
                    throw new Exception("Email and password are required");
                }
                else if (!ValidateEmail(email))
                {
                    throw new Exception("Email is not valid");
                }

                var eStoreContext = new eStoreContext(); // Assuming EStoreContext is the correct class name
                var user = eStoreContext.Members.FirstOrDefault(u => u.Email == email);

                // Validate user existence and password
                if (user == null || string.IsNullOrEmpty(user.Password) || !ValidatePassword(password, user.Password))
                {
                    throw new Exception("Email or password is not valid");
                }

                // Authentication successful
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception($"{ex.Message}");
            }
        }

        public bool Register(string email, string password)
        {
            try
            {
                if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password))
                {
                    throw new Exception("Email and password are required");
                }
                else if (!ValidateEmail(email))
                {
                    throw new Exception("Email is not valid");
                }

                var eStoreContext = new eStoreContext(); // Assuming EStoreContext is the correct class name

                // Check if the email is already registered
                if (eStoreContext.Members.Any(u => u.Email == email))
                {
                    throw new Exception("Email is already registered");
                }

                // Create a new user
                var newUser = new Member
                {
                    Email = email,
                     // You should hash and salt the password for security
                                                      // You may also set other properties of the user here
                };

                // Add the new user to the database
                eStoreContext.Members.Add(newUser);
                eStoreContext.SaveChanges();

                // Registration successful
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception($"{ex.Message}");
            }
        }


        public bool ResetPassword(string email, string newPassword)
        {
            try
            {
                if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(newPassword))
                {
                    throw new Exception("Email and new password are required");
                }
                else if (!ValidateEmail(email))
                {
                    throw new Exception("Email is not valid");
                }

                var eStoreContext = new eStoreContext(); // Assuming EStoreContext is the correct class name
                var user = eStoreContext.Members.FirstOrDefault(u => u.Email == email);

                // Validate user existence
                if (user == null)
                {
                    throw new Exception("User with the specified email not found");
                }

                // Update the password
                user.Password = newPassword; // You should hash and salt the password for security
                eStoreContext.SaveChanges();

                // Password reset successful
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception($"{ex.Message}");
            }
        }



        // Separate method for password validation
        private bool ValidatePassword(string inputPassword, string storedPassword)
        {
            // Kiểm tra xem mật khẩu có ít nhất 3 ký tự không
            if (inputPassword.Length < 3)
            {
                return false;
            }

            // Kiểm tra xem mật khẩu có chứa khoảng trắng không
            if (inputPassword.Contains(" "))
            {
                return false;
            }

            // Kiểm tra xem mật khẩu đã lưu và mật khẩu nhập vào có khớp không
            return inputPassword == storedPassword;
        }


        private bool ValidateEmail(string email)
        {
            if (email.Contains(" "))
            {
                return false;
            }

            // Kiểm tra xem email có ít nhất một ký tự '@' không
            if (!email.Contains("@"))
            {
                return false;
            }

            // Các kiểm tra khác nếu cần thiết

            return true;
        }




    }
}
