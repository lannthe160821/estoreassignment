﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EStoreWPFApp
{
    /// <summary>
    /// Interaction logic for FunctionScreen.xaml
    /// </summary>
    public partial class FunctionScreen : Window
    {
        public FunctionScreen()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ProductManagementWPF productManagementWPF = new ProductManagementWPF();
            this.Visibility = Visibility.Hidden;
            productManagementWPF.Show();
        }

        private void btnOrderManagement_Click(object sender, RoutedEventArgs e)
        {
            OrderManagementWPF orderManagementWPF = new OrderManagementWPF();
            this.Visibility = Visibility.Hidden;
            orderManagementWPF.Show();
        }
    }
}
