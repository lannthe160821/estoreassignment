﻿using EStoreLibrary.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EStoreWPFApp
{
    /// <summary>
    /// Interaction logic for EStoreManagement.xaml
    /// </summary>
    public partial class LoginManagement : Window
    {
        IMenberRepository MenberRepository;
        public LoginManagement(IMenberRepository repository)
        {
            InitializeComponent();
            MenberRepository = repository;
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string email = txtEmail.Text;
                string password = txtPassword.Password; 
                bool LoginSuccess = MenberRepository.Login(email, password);
                if (LoginSuccess)
                {
                    FunctionScreen functionScreen = new FunctionScreen();
                    this.Visibility = Visibility.Hidden;
                    functionScreen.Show();
                }
                else
                {
                    MessageBox.Show("Login failed");

                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnRegister_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnRegister_Click_1(object sender, RoutedEventArgs e)
        {

        }
    }
}
