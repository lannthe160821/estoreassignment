﻿using EStoreLibrary.Repository;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace EStoreWPFApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private IServiceProvider serviceProvider;
        public App()
        {
            ServiceCollection services = new ServiceCollection();
            ConfigureServices(services);
            serviceProvider = services.BuildServiceProvider();
        }
        private void ConfigureServices (ServiceCollection services)
        {
            services.AddSingleton(typeof(IMenberRepository), typeof(MenberRepository));
            services.AddSingleton(typeof(IProductRepository), typeof(ProductRepository));
            services.AddSingleton(typeof(IOrderRepository), typeof(OrderRepository));
            services.AddSingleton<HomeScreen>();
        }

        private void OnStartup (object sender, StartupEventArgs e)
        {
            var eStoreManagement = serviceProvider.GetService<HomeScreen>();
            eStoreManagement.Show();
        }
    }
}
