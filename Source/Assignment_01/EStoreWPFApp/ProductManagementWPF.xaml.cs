﻿using EStoreLibrary.DataAccess;
using EStoreLibrary.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EStoreWPFApp
{
    /// <summary>
    /// Interaction logic for ProductManagementWPF.xaml
    /// </summary>
    public partial class ProductManagementWPF : Window
    {
        IProductRepository productRepository;
  

        public ProductManagementWPF()
        {
            InitializeComponent();
            productRepository = new ProductRepository();
            LoadCategoryList();
        }

        private Product GetProductObject()
        {
            Product? product = null;
            try
            {
                product = new Product
                {
                    ProductId = (txtProductId.Text != "") ? int.Parse(txtProductId.Text) : 0,
                    CategoryId = 1,
                    ProductName = txtProductName.Text,
                    Weight = (txtWeight.Text != "") ? float.Parse(txtWeight.Text) : 0,
                    UnitPrice = int.Parse(txtUnitPrice.Text),
                    UnitInStock = int.Parse(txtUnitInStock.Text)
                };
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Get Product");
            }
            return product;
        }

        public void LoadCategoryList()
        {
            
            cmbCategoryId.ItemsSource = productRepository.GetCategorys();

        }

        public void LoadProductList()
        {
            lvProducts.ItemsSource = productRepository.GetProducts();

        }

        private void btnClose_Click(object sender, RoutedEventArgs e) {

            FunctionScreen functionScreen = new FunctionScreen();
            this.Visibility = Visibility.Hidden;
            functionScreen.Show();
        }


        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LoadProductList();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Load Product List");
            }
        }

        private void btnInsert_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Product Product = GetProductObject();
                productRepository.InsertProduct(Product);
                LoadProductList();
                MessageBox.Show($"{Product.ProductName} inserted successfully", "Insert Product");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Insert Product");

            }
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Product Product = GetProductObject();
                productRepository.UpdateProduct(Product);
                LoadProductList();
                MessageBox.Show($"{Product.ProductName} updated successfully", "Update Product");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Update Product");

            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Product Product = GetProductObject();
                productRepository.DeleteProduct(Product);
                LoadProductList();
                MessageBox.Show($"{Product.ProductName} deleted successfully", "Delete Product");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Delete Product");

            }
        }
    }
}
