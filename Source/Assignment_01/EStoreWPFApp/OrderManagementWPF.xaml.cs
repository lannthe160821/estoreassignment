﻿using EStoreLibrary.DataAccess;
using EStoreLibrary.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EStoreWPFApp
{
    /// <summary>
    /// Interaction logic for OrderManagementWPF.xaml
    /// </summary>
    public partial class OrderManagementWPF : Window
    {
        IOrderRepository orderRepository;
        public OrderManagementWPF()
        {
            InitializeComponent();
            orderRepository = new OrderRepository();
        }

        private Order GetOrderObject()
        {
            Order? order = null;
            try
            {
                order = new Order
                {
                    OrderId = (txtOrderId.Text != "") ? int.Parse(txtOrderId.Text) : 0,
                    MemberId = int.Parse(txtMemberId.Text),
                    OrderDate = dpOrderDate.DisplayDate,
                    RequiredDate = dpRequiredDate.DisplayDate,
                    ShippedDate = dpShippedDate.DisplayDate,
                    Freight = int.Parse(txtFreight.Text)
                };
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Get Order");
            }
            return order;
        }
     
        public void LoadOrderList()
        {
            lvOrders.ItemsSource = orderRepository.GetOrders();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {

            FunctionScreen functionScreen = new FunctionScreen();
            this.Visibility = Visibility.Hidden;
            functionScreen.Show();
        }


        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LoadOrderList();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Load Order List");
            }
        }

        private void btnInsert_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Order order = GetOrderObject();
                orderRepository.InsertOrder(order);
                LoadOrderList();
                MessageBox.Show($" inserted successfully", "Insert Order");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Insert Order");

            }
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Order order = GetOrderObject();
                orderRepository.UpdateOrder(order);
                LoadOrderList();
                MessageBox.Show($"updated successfully", "Update Order");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Update Order");

            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Order order = GetOrderObject();
                orderRepository.DeleteOrder(order);
                LoadOrderList();
                MessageBox.Show($" deleted successfully", "Delete Order");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Delete Order");

            }
        }
    }
}
